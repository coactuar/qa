<?php
require_once "config.php";

    $batch     = mysqli_real_escape_string($link, $_POST['batch']);
    $name     = mysqli_real_escape_string($link, $_POST['usrName']);
    $cntry     = mysqli_real_escape_string($link, $_POST['country']);
    $phone     = mysqli_real_escape_string($link, $_POST['phnNum']);
    
    $query="select * from tbl_users where cntry_code ='$cntry' and mobile_num='$phone' and batch = '$batch'";
    $res = mysqli_query($link, $query) or die(mysqli_error($link)); 
    //echo $query;
    if (mysqli_affected_rows($link) > 0) 
    {
        $row = mysqli_fetch_row($res); 
            
        $login_date   = date('Y/m/d H:i:s');
        $logout_date   = date('Y/m/d H:i:s', time() + 30);
        
        try{
            $today=date("Y/m/d H:i:s");
    
            $dateTimestamp1 = strtotime($row[6]);
            $dateTimestamp2 = strtotime($today);
            //echo $row[8];
            if ($dateTimestamp1 > $dateTimestamp2)
            {
              echo "-1";
            }
            else
            {
              $login_date   = date('Y/m/d H:i:s');
              $logout_date   = date('Y/m/d H:i:s', time() + 30);

              $query="UPDATE tbl_users set login_date='$login_date', logout_date='$logout_date', logout_status='1'  where cntry_code='$cntry' and mobile_num='$phone' and batch ='$batch'";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              
              $_SESSION['user_phone']    = $row[4];
              $_SESSION['user_name']     = $row[2];
              $_SESSION["user_code"]  = $row[3];
              $_SESSION["batch"]  = $row[1];
              echo 'webcast_'.$batch.'.php';
            }
        }
        catch(PDOException $e){
          echo $e->getMessage();
        }
    }
    else{
        
        $login_date   = date('Y/m/d H:i:s');
        $logout_date   = date('Y/m/d H:i:s', time() + 30);

        $query="Insert into tbl_users(batch, name, cntry_code, mobile_num, login_date, logout_date, logout_status, joining_date) values('$batch', '$name','$cntry','$phone','$login_date','$logout_date','1','$login_date') ";
        $res = mysqli_query($link, $query) or die(mysqli_error($link));
        
        $_SESSION['user_phone']    = $phone;
        $_SESSION['user_name']     = $name;
        $_SESSION["user_code"]  = $cntry;
        $_SESSION["batch"]  = $batch;
        echo 'webcast_'.$batch.'.php';
        
    }
?>