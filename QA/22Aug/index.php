<?php
require_once "config.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Freedom From Diabetes Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<link rel="stylesheet" type="text/css" href="css/style1.css">
<script src="https://kit.fontawesome.com/e8d81f325f.js" crossorigin="anonymous"></script>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-12 text-right p-2">
            <img src="img/logo.png" class="img-fluid logo" alt=""/> 
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12 col-md-6 batches">
            <div class="row">
            <div class="col-12 col-md-6 offset-md-3  ">
                    <img src="img/qa22aug.jpg" class="img-fluid" alt=""/> 
                </div>
                <!-- <div class="col-12 col-md-6 ">
                <img src="img/67march.jpg" class="img-fluid" alt=""/> 
                </div>
                <div class="col-12 col-md-6 offset-md-3">
                    <img src="img/68march.jpg" class="img-fluid" alt=""/> 
                </div> -->
               
                <!-- <div class="col-12 col-md-6">
                    <img src="img/65_feb14.jpg" class="img-fluid" alt=""/> 
                </div>
                <div class="col-12 col-md-6">
                    <img src="img/58_58_60_feb14.jpg" class="img-fluid" alt=""/> 
                </div>
                <div class="col-12 col-md-6 ">
                    <img src="img/63_feb14.jpg" class="img-fluid" alt=""/> 
                </div> -->
                <!-- <div class="col-12 col-md-6">
                    <img src="img/58_58_60_feb14.jpg" class="img-fluid" alt=""/> 
                </div>
                <div class="col-12 col-md-6 offset-md-3">
                    <img src="img/63_feb14.jpg" class="img-fluid" alt=""/> 
                </div> -->
                <!-- <div class="col-12 col-md-6">
                    <img src="img/Einsten-Process-Final.jpg" class="img-fluid" alt=""/> 
                </div>
                <div class="col-12 col-md-6">
                    <img src="img/3-GS-1.jpg" class="img-fluid" alt=""/> 
                </div>
                <div class="col-12 col-md-6">
                    <img src="img/Athelete.jpg" class="img-fluid" alt=""/> 
                </div> -->
                
                
            </div>
        </div>
        <div class="col-12 col-md-6">
            <form id="login-form" method="post">
            <h1>Login</h1>
              <div id="login-message"></div>
              <div class="input-group mt-1 mb-1">
               <select id="batch" name="batch" class="form-control" required> 
                <option value="-1">Select Batch </option> 
                <!-- <option value="batch54">Intensive Batch 54</option>
                    <option value="batch55">Intensive Batch 55</option>
                    <option value="batch56">Intensive Batch 56</option>
                    <option value="batch57">Intensive Batch 57</option> -->
                    <!-- <option value="batch58">Intensive Batch 58</option>
                    <option value="batch59">Intensive Batch 59</option>-->
                    <!-- <option value="batch60">Intensive Batch 60</option> 
                    <option value="batch61">Intensive Batch 61</option>
                    <option value="batch62">Intensive Batch 62</option> 
                 <option value="batch63">Intensive Batch 63</option>
                    <option value="batch64">Intensive Batch 64</option> -->
                   <option value="batch65">Default</option> 
                    <!-- <option value="batch66">Intensive Batch 66</option> 
                    <option value="batch67">Intensive Batch 67</option>  
                    <option value="batch68">Intensive Batch 68</option>   -->
                </select>
              </div>
              <div class="input-group mt-1 mb-1">
                <input type="text" class="form-control" placeholder="Enter Name" aria-label="Enter Name" aria-describedby="basic-addon1" name="usrName" id="usrName" required>
              
              </div>
              <div class="input-group mt-1 mb-1">
                <select id="country" name="country" class="form-control" required>
                    <option value="-1">Select Country Code</option>
                    <option value="">India (+91)</option>
                    <?php
                    $query="SELECT * FROM tbl_countries order by country asc";
                    $res = mysqli_query($link, $query) or die(mysqli_error($link)); 
                    while($data = mysqli_fetch_assoc($res))
                    {
                        
                        $country = ($data['country']);
                        if($country !='')
                        {
                     ?>
                     <option value="<?php echo $data['cntry_code']; ?>"><?php echo $country.'(+'.$data['cntry_code'].')'; ?></option>
                     <?php
                        }
                    }
                    ?>
                </select>
              </div>
              <div class="input-group mt-1 mb-1">
                <input type="number" class="form-control" placeholder="Enter Phone Number" aria-label="Enter Phone Number" aria-describedby="basic-addon1" name="phnNum" id="phnNum" required maxlength="11" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
              </div>
              
              <button class="mt-4" type="submit">Login</button>
            </form>
        </div>
    </div>
    <!--<div class="row mt-3 mb-3 batches">
        <div class="col-6 col-md-3">
            <img src="img/batch59.jpg" class="img-fluid" alt=""/> 
        </div>
        <div class="col-6 col-md-3">
            <img src="img/batch58.jpg" class="img-fluid" alt=""/> 
        </div>
        <div class="col-6 col-md-3">
            <img src="img/batch56.jpg" class="img-fluid" alt=""/> 
        </div>
        <div class="col-6 col-md-3">
            <img src="img/batch57.jpg" class="img-fluid" alt=""/> 
        </div>
    </div>-->
    <div class="row mt-3 mb-3">
        <div class="col-12 text-center">
            <div class="icons">
            <a href="https://www.facebook.com/TheFreedomFromDiabetes" target="_blank"><img src="img/036-facebook.svg" alt=""/></a><a href="https://www.youtube.com/user/FreedomFromDiabetes" target="_blank"><img src="img/001-youtube.svg" alt=""/></a><a href="https://www.freedomfromdiabetes.org/" target="_blank" class="web"><img src="img/web.svg" alt=""/></i></a>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script>
$(function(){

  $('.input').focus(function(){
    $(this).parent().find(".label-txt").addClass('label-active');
  });

  $(".input").focusout(function(){
    if ($(this).val() == '') {
      $(this).parent().find(".label-txt").removeClass('label-active');
    };
  });

  $(document).on('submit', '#login-form', function()
{
var mob = $('#phnNum').val();
if(mob < 0){
     alert('Input valid mobile number');
    return false;
}

    if($('#batch').val() == '0')
    {
        alert('Please select your batch');
        return false;
    }
    if($('#country').val() == '-1')
    {
        alert('Please select country code');
        return false;
    }
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
      
      if(data=="-1")
      {
        $('#login-message').text('You are already logged in. Please logout from other location and try again.');
        $('#login-message').addClass('alert-danger');
      }
      else 
      if(data=="0")
      {
        $('#login-message').text('Your phone numer is not registered. Please register.');
        $('#login-message').addClass('alert-danger');
      }
      else
      {
        window.location = data;   
      }
      
  });
  
  return false;
});

});

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-11"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-11');
</script>
</body>
</html>