<?php
	require_once "config.php";
    if(!isset($_SESSION["user_phone"]))
	{
		header("location: ./");
		exit;
	}
    if(($_SESSION["batch"] != 'batch65'))
	{
		header("location: ./");
		exit;
	}

	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $phone=$_SESSION["user_phone"];
            $code=$_SESSION["user_code"];
            $batch=$_SESSION["batch"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where cntry_code='$code' and mobile_num='$phone' and batch ='$batch'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_phone"]);
            unset($_SESSION["user_code"]);
            unset($_SESSION["batch"]);
            
            header("location: ./");
            exit;
        }

    }

	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Freedom From Diabetes Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body class="<?php echo $_SESSION['batch']; ?>">
<div class="container-fluid top-nav">
    <div class="row">
        <div class="col-12 col-md-6 offset-md-3 text-center">
        </div>
        <div class="col-12 col-md-3 text-right">
            <a class="navbar-brand" href="#"><img src="img/logo.png" class="logo"></a>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row mb-1 info">
        <div class="col-12 text-right">
            Hello <?php echo $_SESSION['user_name']; ?>! <a href="?action=logout">Logout</a>
        </div>
    </div>
    <div class="row mt-2">
    <div class="col-12 col-md-6 offset-md-6 text-center">
            <!-- <div class="embed-responsive embed-responsive-16by9 video-panel">
            <iframe src="https://vimeo.com/event/545920/embed/4527611434" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
            </div> -->
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
 
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

  <div class="embed-responsive embed-responsive-16by9 video-panel">
              <iframe class="embed-responsive-item" id="webcast" src="video64.php" allowfullscreen></iframe>
            </div>
			</div>
            <div id="question">
              <div id="question-form" class="panel panel-default">
			  <span style="color:red">You can Submit the questions in the below question box during the Q&A Session.</span>
                  <form method="POST" action="#" class="form panel-body" role="form">
                      <div class="row">
                          <div class="col-12">
                          <div id="ques-message"></div>
                          <div class="form-group">
                          <div class="" >
                         <select name="subject_names"  class=" p-2 "  value="<?php echo $_SESSION['select_batch']; ?>" id="subject_names">
                         <option value="default">Default</option>
                         <!-- <option value="Batch60">Intensive Batch 60</option> -->
                          <!-- <option value="Batch61">Intensive Batch 61</option> -->
                          <!-- <option value="batch65">Deault</option> -->
                          <!-- <option value="Batch63">Intensive Batch 63</option>
                          <option value="Batch64">Intensive Batch 64</option>
                          <option value="Batch65">Intensive Batch 65</option>
                          <option value="Batch66">Intensive Batch 66</option>
                          <option value="Batch67">Intensive Batch 67</option>
                          <option value="Batch68">Intensive Batch 68</option>
                          <option value="Batch69">Intensive Batch 69</option>
                          <option value="Batch70">Intensive Batch 70</option> -->

                         </div>
                              <textarea class="form-control mt-2" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="1"></textarea>
                          </div>
                          
                          </div>
                          <div class="col-12">
                          <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_name']; ?>">
                          <input type="hidden" id="user_phone" name="user_phone" value="<?php echo $_SESSION['user_phone']; ?>">
                          <input type="hidden" id="user_code" name="user_code" value="<?php echo $_SESSION['user_code']; ?>">
                          <input type="hidden" id="user_batch" name="user_batch" value="<?php echo $_SESSION['batch']; ?>">
                          <button class="btn btn-primary btn-sm btn-submit" type="submit">Submit your Question</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
        </div>
        
    </div>
    <div class="row mt-2 mb-2 offset-md-6 top-nav">
        <div class="col-12 text-center">
            <div class="icons">
            <a href="https://www.facebook.com/TheFreedomFromDiabetes" target="_blank"><img src="img/036-facebook.svg" alt=""/></a><a href="https://www.youtube.com/user/FreedomFromDiabetes" target="_blank"><img src="img/001-youtube.svg" alt=""/></a><a href="https://www.freedomfromdiabetes.org/" target="_blank" class="web"><img src="img/web.svg" alt=""/></i></a>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$('#pills-profile-tab').on('click', function(){
   
   $('#pills-profile-tab').hide();
});
$(function(){
     
	$(document).on('submit', '#question-form form', function()
    {  
        if($('#subject_names').val() == '0')
    {
        alert('Please select your batch');
        return false;
    }
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#ques-message').text('Your question is submitted successfully.');
                  $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#ques-message').text(data);
                  $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });
});
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   if(output=="0")
			   {
				   location.href='./';
			   }
         }
});
}
setInterval(function(){ update(); }, 30000);

function changeVideo(video, l)
{
    var vid = '.'+l;
    $('.vid-link').removeClass('act');
    $(vid).addClass('act');
    
    $('#webcast').attr("src",video);
    
    return false;
    
}

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-11"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-11');
</script>

</body>
</html>