<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Admin Login : Live Webcast</title>
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../css/styles.css" rel="stylesheet" type="text/css">
</head>

<body class="admin">
<nav class="navbar navbar-expand-lg navbar-light top-nav">
  <a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>
</nav>
<div class="container-fluid main">
    <div class="row mt-5">
<div class="col-8 col-md-6 col-lg-4 offset-2 offset-md-3 offset-lg-4">
    <div class="login">
<h3>Admin Login</h3>
<form id="login-form" method="post" role="form">
<div id="login-message"></div>
<div class="input-group mb-1">

 
 <select id="batch" name="batch" class="form-control" required>
 <option value="0">Select Batch</option>

  <!-- <option value="batch54">Intensive Batch 54</option>
                    <option value="batch55">Intensive Batch 55</option>
                    <option value="batch56">Intensive Batch 56</option>
                    <option value="batch57">Intensive Batch 57</option> 
                    <option value="batch58">Intensive Batch 58</option>
                    <option value="batch59">Intensive Batch 59</option>
                    <option value="batch60">Intensive Batch 60</option>-->
                     <!-- <option value="batch61">Intensive Batch 61</option>
               <option value="batch62">Intensive Batch 62</option> -->
                    <!--<option value="batch63">Intensive Batch 63</option>
                    <option value="batch64">Intensive Batch 64</option> -->
                    <option value="batch65">Default</option>
                    <!-- <option value="batch66">Intensive Batch 66</option> 
                    <option value="batch67">Intensive Batch 67</option> 
                    <option value="batch68">Intensive Batch 68</option> -->
</select>
</div>

<div class="input-group mb-1">
 <input type="text" class="form-control" placeholder="Username" aria-label="Userame" aria-describedby="basic-addon1" name="loginUser" id="loginUser" required>
</div>

<div class="input-group mb-1">
 <input type="password" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="basic-addon1" name="loginPwd" id="loginPwd" required>
</div>

<div class="input-group">
 <button id="login" class="btn btn-primary btn-sm login-button" type="submit">Login</button>
</div>

    </form>
    </div>

</div>
    </div>
</div>

<script src="../js/jquery.min.js"></script>
<script>
$(function(){

  
  
  $(document).on('submit', '#login-form', function()
{  
if($('#batch').val() == '0')
    {
alert('Please select batch');
return false;
    }
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
      
      if(data=="0")
      {
$('#login-message').text('Invalid login. Please try again.');
$('#login-message').addClass('alert-danger');
      }
      else if(data =='s')
      {
window.location.href='users.php';  
      }
      
  });
  
  return false;
});

});

</script>
<script src="../js/dropdown.js"></script>
</body>
</html>