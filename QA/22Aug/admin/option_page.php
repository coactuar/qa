<?php
	require_once "../config.php";
	
	if((!isset($_SESSION["admin_user"])) || (!isset($_SESSION['admin_batch'])) )
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["admin_user"]);
            unset($_SESSION["admin_batch"]);
            
            header("location: index.php");
            exit;
        }

    }
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>


<div class="container-fluid">
     <div class="row login-info links">   
      
    <div class="row mt-1">
        <div class="col-4">
            <a href="export_logins.php"><img src="excel.png" height="45" alt=""/></a>
        </div>
        <div class="col-4">



       
           
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-12">
            <div id="users"> </div>
        </div>
    </div>
</div>


<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
    getUsers('1');
});

function update(pageNum)
{
  getUsers(pageNum);
}

function getUsers(pageNum)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getusers', page: pageNum},
        type: 'post',
        success: function(response) {
            
            $("#users").html(response);    
        }
    });
}

</script>

</body>
</html>